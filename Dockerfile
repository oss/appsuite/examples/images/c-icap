FROM debian:bookworm-slim
LABEL maintainer="Sebastian Lutz <sebastian.lutz@open-xchange.com>"

# Set the working directory
WORKDIR /root

# Install c-icap and necessary build dependencies
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
            c-icap \
            libicapapi-dev \
            build-essential \
            libssl-dev \
            wget \
    && apt-mark auto \
            libicapapi-dev \
            build-essential \
            libssl-dev \
            wget \
    && rm -rf /var/lib/apt/lists/*

# Set the working directory
WORKDIR /build

# Download squidclamav-7.2
RUN wget http://downloads.sourceforge.net/project/squidclamav/squidclamav/7.2/squidclamav-7.2.tar.gz \
    && gunzip squidclamav-7.2.tar.gz \
    && tar -xvf squidclamav-7.2.tar

# Configure and build the package
WORKDIR /build/squidclamav-7.2
RUN ./configure --with-c-icap=/usr \
    && make \
    && make install

# Create the directory for c-icap to store its runtime files (e.g., pid file)
RUN mkdir -p /run/c-icap \
    # Set ownership of the c-icap runtime directory to the c-icap user and group
    && chown c-icap:c-icap /run/c-icap/

# Some cleanup
RUN apt-get autoremove --purge -y \
    	&& apt-get clean \
    	&& rm -rf /var/tmp/* /tmp/* /var/lib/apt/cache/* \
        && rm -rf /build

CMD ["c-icap", "-D", "-N", "-f", "/etc/c-icap/c-icap.conf"]