# Open-Xchange Anti-Virus Example Docker Image

This repository provides a Dockerfile using [SquidClamav](https://squidclamav.darold.net/) as an ICAP service through the c-icap server.

## License

This project uses the following license: Apache-2.0